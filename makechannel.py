from yt_dlp import YoutubeDL
from simplejson import dump
from sys import argv
from time import time

url = argv[1]
channelname = argv[2]
filepath = f"channels/{channelname}.json"

json = {
	"name": channelname,
	"url": url,
	"videos": []
}

with YoutubeDL() as ydl:
	info = ydl.extract_info(url, download=False, process=False)
	for video in info["entries"]:
		if not video.get("duration"):
			# the videos with missing duration seem to all be youtube shorts
			print(f"{video['id']} {video['title']} missing duration")
			continue
		json["videos"].append({
			"id": video["id"],
			"title": video["title"],
			"duration": video["duration"]
		})

json["timestamp"] = int(time() * 1000)

with open(filepath, "w") as f:
		dump(json, f)
	

